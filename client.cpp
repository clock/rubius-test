/*
 * Client for daemon, which launch lyapas encryption/decryption for data
 */

/*
 * Using: ./client -e[-d] [/path/to/keys] /path/to/text/
 * -e[-d] - шифрование или расшифрование
 */

/*
 * https://stackoverflow.com/questions/22370036/socket-programming-in-c-sending-a-file-from-server-to-client
 * https://stackoverflow.com/questions/11952898/c-send-and-receive-file
 * https://habrahabr.ru/post/129207/
 * https://stackoverflow.com/questions/17954432/creating-a-daemon-in-linux
 */

#include <iostream>
#include <cstdlib>
#include <cstdio>
#include <sys/sendfile.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <cstring>
#include <unistd.h>
#include <netdb.h>
#include <fcntl.h>


// #include <netinet/in.h>

// DONE:

// TODO:
//		1. Оконное приложение (клиент). Отправляет
//+		 текстовый файл и некоторое слово на сервер,
//+		 отображает возвращённое сервером число вхождений
//+		 указанного слова в переданный файл.
//		2. разместить на форме окна соответствующие
//+		 компоненты для выбора файла, ввода слова,
//+		 отправки данных на сервер, отображения результата.
//		3. Запрещать пользователю повторно отправлять
//+		 данные на сервер, пока не будет получен ответ
//+		 на предыдущий запрос.
//		4. установить фильтр на выбор файлов по расширению
//+		 (например *.txt), а так же установить ограничение
//+		 (на вводимый текст (позволять вводить только буквы
//+		 (и цифры).
//		5. Будет плюсом, если для сборки проекта будет
//+		 использоваться CMake.
//		6. Будет плюсом наличие модульных тестов
//+		 (Желательно с использованием GoogleTest).

// using std::;
using std::cout;
using std::cin;

// char *host = "127.0.0.1";
// char *hostname = "localhost";
const char *host = "127.0.0.1";
const char *hostname = "localhost";
int port = 0xbabe;

void error(const char *msg){
	perror(msg);
	exit(0);
}

// echo stuff only for debugging and testing
int echo(int sock){
	int n, bufLen = 256;
	char buffer[bufLen];
	cout << "Please enter the message: ";
// 	bzero(buffer, bufLen);
	memset(buffer, 0, bufLen);
	fgets(buffer, (bufLen-1), stdin);
	n = write(sock, buffer, strlen(buffer));
	if (n < 0) 
		error("echo ERROR: writing to socket");
// 	bzero(buffer, bufLen);
	memset(buffer, 0, bufLen);
	n = read(sock, buffer, (bufLen-1));
	if (n < 0) 
		error("echo ERROR: reading from socket");
	cout << buffer;
	return 0;
}

int sendFiles(int sock, char *keys, char *text){
	int textFile = open(text, O_RDONLY);
	int keysFile = 0;
	int bufLen = 20, bytesSent, remaining, pieceOfFile = 1024;
	char buffer[bufLen];
	long int offset;
	bytesSent = offset = remaining = 0;
	struct stat fileStat;
	if(textFile == -1){
		error("sendFiles ERROR: opening text file to send");
		return 1;
	}
	if(keys != NULL){
		keysFile = open(keys, O_RDONLY);
		if(keysFile == -1){
			error("sendFiles ERROR: opening keys file to send");
			return 1;
		}
		if(fstat(keysFile, &fileStat) < 0)
			error("sendFiles ERROR: fstat when keysFile");
// 		bytesRead = fread(sendBuffer, bufLen, 1, keysFile);
// 		bytesSent = send(sock, sendBuffer, bytesRead, 0);
		sprintf(buffer, "%ld", fileStat.st_size);
		bytesSent = send(sock, buffer, strlen(buffer), 0);
		if(bytesSent < 0)
			error("sendFiles ERROR: on sending size of keysFile");
		puts("Sending keys...");
		remaining = fileStat.st_size;
		while( (remaining > 0) && ((bytesSent = sendfile(sock, keysFile, &offset, pieceOfFile)) > 0) ){
			remaining -= bytesSent;
			fprintf(stdout, "%d bytes sent already. Currently offset is: %ld and remaining data = %d\n", bytesSent, offset, remaining);
		}
	}
	if(fstat(textFile, &fileStat) < 0)
			error("sendFiles ERROR: fstat when textFile");
// 	bytesRead = fread(sendBuffer, bufLen, 1, textFile);
	sprintf(buffer, "%ld", fileStat.st_size);
	bytesSent = send(sock, buffer, strlen(buffer), 0);
	if(bytesSent < 0)
		error("sendFiles ERROR: on sending size of textFile");
	puts("Sending text...");
	remaining = fileStat.st_size;
	while( (remaining > 0) && ((bytesSent = sendfile(sock, textFile, &offset, pieceOfFile)) > 0) ){
		remaining -= bytesSent;
		fprintf(stdout, "%d bytes sent already. Currently offset is: %ld and remaining data = %d\n", bytesSent, offset, remaining);
	}
	return 0;
}

int client(char **args, int count){
	int sockfd, n;
	struct sockaddr_in serv_addr;
	struct hostent *server;
	sockfd = socket(AF_INET, SOCK_STREAM, 0);
	if(sockfd < 0)
		error("sendFiles ERROR: opening socket");
	server = gethostbyname(hostname);
	if(server == NULL)
		error("Host wasn\'t resolve");
// 	bzero((char *) &serv_addr, sizeof(serv_addr));
	memset((char *) &serv_addr, 0, sizeof(serv_addr));
	serv_addr.sin_family = AF_INET;
	bcopy((char *)server->h_addr, (char *)&serv_addr.sin_addr.s_addr, 
		  server->h_length);
	serv_addr.sin_port = htons(port);
	n = sizeof(serv_addr);
	if(connect(sockfd, (struct sockaddr *) &serv_addr, n) < 0)
		error("sendFiles ERROR: connecting");
	
// 	should be my stuff with sending data for encryption/decryption here
	echo(sockfd);
// 	
	/*
	if( !(strcmp(args[1], "-e")) ){
//		TODO: добавить проверку корректности args[2]
		n = send(sockfd, "encrypt", strlen("encrypt"), 0);
		if (n < 0) 
			error("ERROR writing to socket before encrypt");
		sendFiles(sockfd, NULL, args[2]);
		encrypt();
	}
	else{
// 		TODO: добавить проверку корректности args[2] и args[3]
		n = send(sockfd, "decrypt", strlen("decrypt"), 0);
		if (n < 0) 
			error("ERROR writing to socket before decrypt");
		sendFiles(sockfd, args[2], args[3]);
		decrypt();
	}
	*/
	close(sockfd);
	return 0;
}


int main(int argc, char **argv){
// 	int check = strcmp(argv[1], "-e");
// 	int fail = 1;
	/*
	if( 2 == strlen(argv[1]) ){
		if( !(strcmp(argv[1], "-e") ) && (argc > 2) )
			fail = 0;
		else if( !(strcmp(argv[1], "-d") ) && (argc > 3) )
			fail = 0;
	}
	if(fail == 1){
		puts("Using: ./client -e[-d] [/path/to/keys] /path/to/text/");
		error("ERROR too few arguments or invalid argument");
	}
	*/
	client(argv, argc);
	return 0;
}