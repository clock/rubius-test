#ifndef RTESTSERVER_H
#define RTESTSERVER_H

#include <QTcpServer>
#include <QTcpSocket>
//#include <QProcess>
#include <QRegExp>
#include <QDebug>
#include <QTime>
#include <QFile>
#include <QSet>

class rtserver : public QTcpServer{
	Q_OBJECT
	public:
		rtserver(QObject *parent=0);
	private slots:
		void newConnection();
		void readyStuff();
		void disconnected();
//	signals:
//		void fileReceived();
	protected:
//		void incomingConnection(qintptr);
		void receiveFile(QString const, QTcpSocket * const);
	private:
		quint32 countMatchesInFile(QString, QString);
//		QSet<QTcpSocket*> clients;
		QHash<QTcpSocket*, QByteArray*> buffers; // clients' file datas
		QHash<QTcpSocket*, QString*> fileNames; // clients' file names
		QHash<QTcpSocket*, qint32*> sizes; // clients' file sizes
};

#endif // RTESTSERVER_H
