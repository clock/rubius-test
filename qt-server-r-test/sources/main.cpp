#include <QCoreApplication>
#include <QTextStream>
#include <QDebug>
#include "../headers/r-test-server.h"

//server
// DONE:
//		1. Консольное приложение (сервер). Слушает некоторый
//+		 порт, принимает входящие TCP-соединения, по
//+		 установленному соединению принимает от клиента
//+		 текстовый файл и некоторое слово, вычисляет
//+		 кол-во вхождений слова в файле, возвращает
//+		 результат клиенту.
//		2. Желательно обеспечить возможность работы с
//+		 несколькими клиентами.

// TODO:
//		3. Будет плюсом, если для сборки проекта будет
//+		 использоваться CMake.
//		4. Будет плюсом наличие модульных тестов
//+		 (Желательно с использованием GoogleTest).
//		5. поставить ограничитель на спецсимволы в countmatchesinfile





int main(int argc, char *argv[])
{
	QCoreApplication a(argc, argv);
	rtserver *server = new rtserver();
	quint16 port = 0xbabe; // 47806
//	QTextStream qin(stdin), qout(stdout); // qout Не работает
//	qout << "Enter port: ";
//	qin >> port;
//	qDebug() << port;
//	Проверка порта, что не перекрывает системные
	bool isSuccess = server->listen(QHostAddress::Any, port);
	if( !isSuccess ){
		/*
		QString error("Could not listen on port ");
		error += QString::number(port);
		return 0;
		*/
		qFatal("Could not listen on port");
	}
//	qout << "success";
	qDebug() << "success";
	return a.exec();
}
