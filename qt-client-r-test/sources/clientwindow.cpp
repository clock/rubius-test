#include "../headers/clientwindow.h"



clientWindow::clientWindow(QWidget *parent) : QMainWindow(parent){
	/*
	valid1 = new QRegularExpressionValidator(QRegularExpression( "^[A-Za-z0-9]+$"));//"\\w+(\\d+)"));
	valid2 = new QRegularExpressionValidator(QRegularExpression( "^[A-Za-z0-9]+$"));//"\\S+(\\w+.(txt))$"));
	word_for_searching->setValidator(valid1);
	file_path->setValidator(valid2);
	*/
	setupUi(this);
	port = 0xbabe;
	ip = "127.0.0.1";
	ipAddr->setPlaceholderText(ip);
	portLine->setPlaceholderText(QString::number(port));
	stackedWidget->setCurrentWidget(paramPage);
	socket = new QTcpSocket(this);
//	connect(socket, SIGNAL(readyRead()), this, SLOT(readyStuff()));
//	connect(socket, SIGNAL(fileReceived()), this, SLOT(catchRequest()));
//	connect(socket, SIGNAL(textChanged()), this, SLOT(checkLineEditWord()));
	connect(socket, SIGNAL(disconnected()), this, SLOT(disconnected()));
	connect(socket, SIGNAL(connected()), this, SLOT(connected()));
}

clientWindow::~clientWindow(){
	/*
	delete valid1;
	delete valid2;
	*/
}

//clientWindow::~clientWindow(){
//	delete ui;
//}


//void clientWindow::checkLineEditWord(){}

void clientWindow::on_butChooseFile_clicked(){
	QString path = file_path->text();
	if( !(path.size()) )
		path = "/";
	path = QFileDialog::getOpenFileName(this,
		tr("Open Text File"), path, tr("Text Files (*.txt)"));
//	qDebug() << progressBar->value();
	if( path.size() )
		file_path->setText(path);
}

bool clientWindow::sendData(QByteArray data){
	if(socket->state() == QAbstractSocket::ConnectedState){
		socket->write(data); // отправка данных
		return socket->waitForBytesWritten(3000);
	}
	else
		return false;
}

void clientWindow::sendFile(QString filePath){
	QFile toSend(filePath);

	if(toSend.open(QIODevice::ReadOnly)){
		QFileInfo fileInfo(filePath);
		qint64 sent, remain = fileInfo.size();
		QByteArray rawFile;

		sendData(QByteArray::number(fileInfo.size()));
		qDebug() << "fileInfo.size(): " << fileInfo.size();
		progressBar->setMaximum(remain+(remain/15));
		qDebug() << "all progress: " << remain+(remain/15);
		sent = 0;
		do{
			rawFile = toSend.read(512);
			sent += rawFile.size();
			progressBar->setValue(sent);
			sendData(rawFile);
			qDebug() << "Sent: "<< sent;
			qDebug() << "ToSend: "<< remain-sent;
		}while( !toSend.atEnd() );
		toSend.close();
	}else
		qDebug() << "sendFile: File wasn't open: ";
//	qDebug() << fileInfo.fileName() << ' ' << fileInfo.filePath();
}

void clientWindow::on_butSend_clicked(){
	QStringList tmp;
	if( !(file_path->text().trimmed().size()) ){
		QMessageBox::warning(this, "ERROR", "Looks like file path is empty or incorrect\n\rYou need to enter path to text file");
		return;
	}
	tmp = file_path->text().split('/').last().split('.');
	if( (tmp.first(). size() < 0) && (tmp.last() != "txt") ){
//	if( file_path->text().right(4) != ".txt" ){
		QMessageBox::warning(this, "ERROR", "Looks like incorrect file\'s name or it\'s type");
		return;
	}
	if( !(word_for_searching->text().trimmed().size()) ){ // добавить обработку недопустимых символов
		QMessageBox::warning(this, "ERROR", "It's required to set word for searching. Acceptable symbols: letters and numbers");
		return;
	}
	setBlockInterface(true);
//	qDebug() << (char *)word_for_searching->text().data() << ' ' << word_for_searching->text().size();
	if( socket->waitForConnected() ){
		socket->write(word_for_searching->text().toUtf8());
		socket->waitForBytesWritten();
		qDebug() << "Word sent";
	} else
		qDebug() << "Timeout";
	sendFile(file_path->text());
	catchRequest();
//	qDebug() << tmp.first() << ' ' << tmp.last();
//	qDebug() << file_path->text().right(4);
//	labelStatusState->setText("send pushed");
//	progressBar->setValue(0);
}

void clientWindow::loading(){
	qint32 i, j, tmp;
	QString sLoading("Loading"), sForLabelLoading("       ");
	/*
	while(1){
		labelLoading->setText("L");
		labelLoading->setText("Lo");
		labelLoading->setText("Loa");
		labelLoading->setText(" oad");
		labelLoading->setText("  adi");
		labelLoading->setText("   din");
		labelLoading->setText("    ing");
		labelLoading->setText("     ng");
		labelLoading->setText("      g");
		labelLoading->setText("");
	}
	*/
	// здесь должен быть прикольный эффект загрузки, но у меня не получилось =[[
	tmp = i = j = 0;
	labelLoading->setText(sLoading);
		while( tmp < 10 ){ // tmp для отладки, проверить работу цикла, в конечном итоге должно быть условие
//							 установки соединения
		if(i < 6){
			sForLabelLoading[i] = sLoading[i];
			++i;
		}
		else
			if(j > 5)
				i = j = 0;
		if(i > 2){
			sLoading[j] = ' ';
			++j;
		}
		labelLoading->setText(sForLabelLoading);
		++tmp;
	}
}

void clientWindow::setIP(QString newIP){
	ip = newIP;
	return;
}

void clientWindow::setPort(QString newPort){
	port = newPort.toInt();
	return;
}


void clientWindow::on_butConnect_clicked(){
	QMessageBox::StandardButton reply;
	// добавить обработку некорректных ip адресов
//	в окошке поменять местами yes и no
	if(ipAddr->text().size() < 1){ //if(ipAddr->text().size() < 1){
		reply = QMessageBox::question(this, "WARNING", "You did not enter ip address or domain\r\n\"Yes\" - you will continue and bind to local ip address.\r\n\"No\" - to correct this", QMessageBox::Yes|QMessageBox::No);
		if( reply == QMessageBox::No)
			return;
	}
	else
		ip = ipAddr->text();
//		setIP(ipAddr->text());
	if(portLine->text().size() > 0)
		port = portLine->text().toInt();
//		setPort(portLine->text());
	else{
		reply = QMessageBox::question(this, "WARNING", "You did not enter port number\r\n\"Yes\" - you will continue and bind on 47806 port.\r\n\"No\" - to correct this", QMessageBox::Yes | QMessageBox::No);
	if( reply == QMessageBox::No)
		return;
	}
	stackedWidget->setCurrentWidget(loadingPage);
//	stackedWidget->setCurrentWidget(appWindow);
	qDebug() << ip << ' ' << port;
	socket->connectToHost(ip, port);
//	qDebug() << socket->isOpen();
//	loading();
}

//void clientWindow::readyStuff(){}

void clientWindow::catchRequest(){
	QString matchesCount;
//	socket->waitForBytesWritten(2000);
//	socket->waitForReadyRead(2000);
	if( socket->waitForReadyRead() ){
		qDebug() << socket->bytesAvailable();
		qDebug() << "Catching";
		matchesCount = QString(socket->readAll().trimmed());
		qDebug() << matchesCount;
		if(!matchesCount.isEmpty())
			progressBar->setValue(progressBar->maximum());
		labelCountMatches->setText(matchesCount);
	}else
		qDebug() << "Timeout";
	setBlockInterface(false);
}

void clientWindow::connected(){
	progressBar->setValue(0);
	if(stackedWidget->currentWidget() != appWindow)
		stackedWidget->setCurrentWidget(appWindow);
	labelStatusState->setText("Connected");
	word_for_searching->setPlaceholderText(QString("Your word for searching"));
	file_path->setPlaceholderText(QString("Path to file"));
	stackedWidgetStatus->setCurrentWidget(pageConnected);
}

void clientWindow::setBlockInterface(bool value){
	word_for_searching->setEnabled(!value);
	file_path->setEnabled(!value);
	butChooseFile->setEnabled(!value);
	butSend->setEnabled(!value);
	
}

void clientWindow::disconnected(){
	labelStatusState->setText("Disconnected");
	stackedWidgetStatus->setCurrentWidget(pageDisconnected);
}

