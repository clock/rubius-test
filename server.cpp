/*
 server receive file and count every string matches in it
 
 
*/
#include <iostream>
#include <cstdlib>
#include <cstdio>
#include <sys/socket.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <cstring>
#include <unistd.h>
#include <netdb.h>

// DONE:


// TODO:
//		1. Консольное приложение (сервер). Слушает некоторый
//+		 порт, принимает входящие TCP-соединения, по
//+		 установленному соединению принимает от клиента
//+		 текстовый файл и некоторое слово, вычисляет
//+		 кол-во вхождений слова в файле, возвращает
//+		 результат клиенту.
//		2. Желательно обеспечить возможность работы с
//+		 несколькими клиентами.
//		3. Будет плюсом, если для сборки проекта будет
//+		 использоваться CMake.
//		4. Будет плюсом наличие модульных тестов
//+		 (Желательно с использованием GoogleTest).

// using std::;
using std::cout;
using std::cin;

// char *host = "127.0.0.1";
// char *hostname = "localhost";
const char *host = "127.0.0.1";
const char *hostname = "localhost";
int port = 0xbabe;


void error(const char *msg){
	perror(msg);
	exit(1);
}


int cpString(char *string1, const char *string2){
	int i, n = strlen(string2);
	for(i = 0; i < n; ++i)
		string1[i] = string2[i];
// 	cout << "[DEBUG] cpString: " << string1);
	return 0;
}

int pushBackString(char *string1, const char *string2){
	int j, i, n = strlen(string2);
	for(i = strlen(string1), j = 0; j < n; ++i, ++j)
		string1[i] = string2[j];
// 	cout << "[DEBUG] pushBackString: " << string1);
	return 0;
}

int pushForwardString(char *string1, const char *string2){
	int i32BufSize = strlen(string1) + strlen(string2);
	char buffer[i32BufSize + 2];
	memset(buffer, 0, (i32BufSize + 2));
	cpString(buffer, string2);
// 	cout << "[DEBUG] pushForwardString: " << buffer);
	pushBackString(buffer, string1);
// 	cout << "[DEBUG] pushForwardString: " << buffer);
	cpString(string1, buffer);
// 	cout << "[DEBUG] pushForwardString: " << string1);
	return 0;
}

// just for debug and testing
int echo(int sock){
	int n;
	unsigned int ui32BufLen = 256;
	char buffer[ui32BufLen];
	const char *messageWas = "Your message was: ";
// 	bzero(buffer, ui32BufLen);
	memset(buffer, 0, ui32BufLen);
	n = read(sock, buffer, (ui32BufLen-1));
	if (n < 0)
		error("echo ERROR: reading from socket");
	cout << "Here is the message: " << buffer);
	if( (strlen(messageWas) + strlen(buffer)) > ui32BufLen )
		error("echo ERROR: new buffer\'s size greater than old size");
// 	cout << "[DEBUG] echo: " << buffer);
	pushForwardString(buffer, messageWas);
	n = strlen(buffer);
	n = write(sock, buffer, n);
	if (n < 0)
		error("echo ERROR: writing to socket");
	return 0;
}

int receiveFiles(int sock, int keys, int text){
	FILE *keysFile = NULL;
	FILE *textFile = NULL;
	int bufLen = 1025, bytesRecvd, remaining, pid = getpid();
	bytesRecvd = remaining = 0;
	char buffer[bufLen];
	struct stat dirStat;
	if(stat("/tmp/.dexter/", &dirStat) == -1)
		mkdir("/tmp/.dexter/", 0700);
	if(keys != 0){
		recv(sock, buffer, bufLen, 0);
        remaining = atoi(buffer);
		if(remaining > 0){
			sprintf(buffer, "/tmp/.dexter/keys%d.txt", pid);
			//fprintf(stdout, "%s", buffer);
			keysFile = fopen(buffer, "w");
			if(keysFile == NULL){
				error("receiveFiles ERROR: opening keys file for receive");
				return 1;
			}
	// 		bytesRead = read(sock, buffer, bufLen);
			while( (remaining > 0) && ( (bytesRecvd = recv(sock, buffer, bufLen, 0) ) > 0) ){
				fwrite(buffer, sizeof(char), bytesRecvd, keysFile);
				remaining -= bytesRecvd;
			}
			fclose(keysFile);
		}
	}
	recv(sock, buffer, bufLen, 0);
	remaining = atoi(buffer);
	if(remaining > 0){
		sprintf(buffer, "/tmp/.dexter/text%d.txt", pid);
		//fprintf(stdout, "%s", buffer);
		textFile = fopen(buffer, "w");
		if(textFile == NULL){
			error("receiveFiles ERROR: opening text file for receive");
			return 1;
		}
	// 	bytesRead = read(sock, buffer, bufLen);
		while( (remaining > 0) && ( (bytesRecvd = recv(sock, buffer, bufLen, 0) ) > 0) ){
				fwrite(buffer, sizeof(char), bytesRecvd, textFile);
				remaining -= bytesRecvd;
		}
		fclose(textFile);
	}
	return 0;
}

// encryption actions
int descendant(int sock){
	int bufLen = 256;
	char buffer[bufLen];
// 	bzero(buffer, bufLen);
	memset(buffer, 0, bufLen);
// 	n = read(sock, buffer, (bufLen-1));
// 	if (n < 0)
// 		error("descendant ERROR: reading from socket");
	/*
	if( !(strcmp(buffer, "encrypt")) ){
		// Encryption
		receiveFiles(sock, 0, 1);
		
	}
	else{
		// Decryption
		receiveFiles(sock, 1, 1);
		
	}
	*/
	echo(sock);
	return 0;
}

// server
int ancestor(){
	int sockfd, newsock, n, pid;
	socklen_t clilen;
	struct sockaddr_in serv_addr, cli_addr;
	sockfd = socket(AF_INET, SOCK_STREAM, 0);
	if (sockfd < 0) 
		error("ancestor ERROR: opening socket");
// 	bzero((char *) &serv_addr, sizeof(serv_addr));
	memset((char *) &serv_addr, 0, sizeof(serv_addr));
	serv_addr.sin_family = AF_INET;
	serv_addr.sin_addr.s_addr = INADDR_ANY;
	serv_addr.sin_port = htons(port);
	n = sizeof(serv_addr);
	if (bind(sockfd, (struct sockaddr *) &serv_addr, n) < 0) 
		error("ancestor ERROR: on binding");
	listen(sockfd, 10);
	clilen = sizeof(cli_addr);
	while(1){
		newsock = accept(sockfd, (struct sockaddr *) &cli_addr, &clilen);
		if (newsock < 0)
			error("ancestor ERROR: on accept");
		pid = fork();
		if(pid < 0)
			error("ancestor ERROR: on fork");
		if(!pid){
			descendant(newsock);
			close(newsock);
		}
	}
	close(sockfd);
	return 0; 
}


int main(){
	
	ancestor();
	
	return 0;
}