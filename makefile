all: client server

client-qt:
	g++ -std=c++0x -ggdb -g3 -Wall -o cli client-qt.cpp

server-qt:
	g++ -std=c++0x -ggdb -g3 -Wall -o ser server-qt.cpp

client:
	g++ -std=c++0x -ggdb -g3 -Wall -o cli client.cpp

server:
	g++ -std=c++0x -ggdb -g3 -Wall -o ser server.cpp
